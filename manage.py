#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":

    # if 'dev' in sys.argv:
    #     print ("in dev")
    #     os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orchard_crm.settings.dev")
    #     sys.argv.remove('dev')
    # elif 'prod' in  sys.argv:
    #     os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orchard_crm.settings.prod")
    #     sys.argv.remove('prod')
    # elif 'test' in  sys.argv:
    #     os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orchard_crm.settings.test")
    #     sys.argv.remove('test')
    # else:
    #     raise ValueError("You need to provide developement env like \n" 
    #         "./manage.py runserver dev \n"
    #         "./manage.py runserver prod \n"
    #         )
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "orchard_crm.settings.dev")

    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
