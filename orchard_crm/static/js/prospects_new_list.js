Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}

if(localStorage['zip_rule'] == null){
	localStorage.setItem('zip_rule', JSON.stringify({"$or": []}));
}else{
	localStorage['zip_rule'] = JSON.stringify({"$or": []});
}

if(localStorage['query'] == null){
	localStorage.setItem('query', null);
}else{
	localStorage['query'] = null;
}

if(localStorage['county_rule'] == null){
	localStorage.setItem('county_rule', JSON.stringify({"$or": []}));
}else{
	localStorage['county_rule'] = JSON.stringify({"$or": []});
}

if(localStorage['county_list'] == null){
	localStorage.setItem('county_list', []);
}else{
	localStorage['county_list'] = [];
}

if(localStorage['state_rule'] == null){
	localStorage.setItem('state_rule', JSON.stringify({"$or": []}));
}else{
	localStorage['state_rule'] = JSON.stringify({"$or": []});
}

if(localStorage['county_state_rule'] == null){
	localStorage.setItem('county_state_rule', JSON.stringify({"$or": []}));
}else{
	localStorage['county_state_rule'] = JSON.stringify({"$or": []});
}

// move to fips-query.js
var usStates = {
    'ALABAMA': 'AL',
    'ALASKA': 'AK',
    'AMERICAN SAMOA': 'AS',
    'ARIZONA': 'AZ',
    'ARKANSAS': 'AR',
    'CALIFORNIA': 'CA',
    'COLORADO': 'CO',
    'CONNECTICUT': 'CT',
    'DELAWARE': 'DE',
    'DISTRICT OF COLUMBIA': 'DC',
    'FEDERATED STATES OF MICRONESIA': 'FM',
    'FLORIDA': 'FL',
    'GEORGIA': 'GA',
    'GUAM': 'GU',
    'HAWAII': 'HI',
    'IDAHO': 'ID',
    'ILLINOIS': 'IL',
    'INDIANA': 'IN',
    'IOWA': 'IA',
    'KANSAS': 'KS',
    'KENTUCKY': 'KY',
    'LOUISIANA': 'LA',
    'MAINE': 'ME',
    'MARSHALL ISLANDS': 'MH',
    'MARYLAND': 'MD',
    'MASSACHUSETTS': 'MA',
    'MICHIGAN': 'MI',
    'MINNESOTA': 'MN',
    'MISSISSIPPI': 'MS',
    'MISSOURI': 'MO',
    'MONTANA': 'MT',
    'NEBRASKA': 'NE',
    'NEVADA': 'NV',
    'NEW HAMPSHIRE': 'NH',
    'NEW JERSEY': 'NJ',
    'NEW MEXICO': 'NM',
    'NEW YORK': 'NY',
    'NORTH CAROLINA': 'NC',
    'NORTH DAKOTA': 'ND',
    'NORTHERN MARIANA ISLANDS': 'MP',
    'OHIO': 'OH',
    'OKLAHOMA': 'OK',
    'OREGON': 'OR',
    'PALAU': 'PW',
    'PENNSYLVANIA': 'PA',
    'PUERTO RICO': 'PR',
    'RHODE ISLAND': 'RI',
    'SOUTH CAROLINA': 'SC',
    'SOUTH DAKOTA': 'SD',
    'TENNESSEE': 'TN',
    'TEXAS': 'TX',
    'UTAH': 'UT',
    'VERMONT': 'VT',
    'VIRGIN ISLANDS': 'VI',
    'VIRGINIA': 'VA',
    'WASHINGTON': 'WA',
    'WEST VIRGINIA': 'WV',
    'WISCONSIN': 'WI',
    'WYOMING': 'WY' 
}

var STATE_FIPS = {
    "ALABAMA": "01",
    "ALASKA": "02",
    "ARIZONA": "04",
    "ARKANSAS": "05",
    "CALIFORNIA": "06",
    "COLORADO": "08",
    "CONNECTICUT": "09",
    "DELAWARE": "10",
    "DISTRICT OF COLUMBIA": "11",
    "FLORIDA": "12",
    "GEORGIA": "13",
    "HAWAII": "15",
    "IDAHO": "16",
    "ILLINOIS": "17",
    "INDIANA": "18",
    "IOWA": "19",
    "KANSAS": "20",
    "KENTUCKY": "21",
    "LOUISIANA": "22",
    "MAINE": "23",
    "MARYLAND": "24",
    "MASSACHUSETTS": "25",
    "MICHIGAN": "26",
    "MINNESOTA": "27",
    "MISSISSIPPI": "28",
    "MISSOURI": "29",
    "MONTANA": "30",
    "NEBRASKA": "31",
    "NEVADA": "32",
    "NEW HAMPSHIRE": "33",
    "NEW JERSEY": "34",
    "NEW MEXICO": "35",
    "NEW YORK": "36",
    "NORTH CAROLINA": "37",
    "NORTH DAKOTA": "38",
    "OHIO": "39",
    "OKLAHOMA": "40",
    "OREGON": "41",
    "PENNSYLVANIA": "42",
    "PUERTO RICO": "72",
    "RHODE ISLAND": "44",
    "SOUTH CAROLINA": "45",
    "SOUTH DAKOTA": "46",
    "TENNESSEE": "47",
    "TEXAS": "48",
    "UTAH": "49",
    "VERMONT": "50",
    "VIRGINIA": "51",
    "WASHINGTON": "53",
    "WEST VIRGINIA": "54",
    "WISCONSIN": "55",
    "WYOMING": "56"
}

var FIPS_STATE = {}
    
for (name in STATE_FIPS) {
    var fips = STATE_FIPS[name];
    FIPS_STATE[fips] = usStates[name];
}


$(function(){
	function escapeRegExp(str) {
	    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}

	var mopers = {
		equal:            function(v){ o={};o[v[0]]=v[1];return o; },
	    not_equal:        function(v){ o={};o[v[0]]={'$ne':v[1]};return o; },
	    //in:               function(v){ o={};o[v[0]]={'$in':v[1]};return o; },
	    //not_in:           function(v){ o={};o[v[0]]={'$nin':v[1]};return o; },
	    less:             function(v){ o={};o[v[0]]={'$lt':v[1]};return o; },
	    less_or_equal:    function(v){ o={};o[v[0]]={'$lte':v[1]};return o; },
	    greater:          function(v){ o={};o[v[0]]={'$gt': v[1]};return o; },
	    greater_or_equal: function(v){ o={};o[v[0]]={'$gte': v[1]};return o; },
	    //between:          function(v){ return {'$gte': v[0]:, '$lte': v[1]}; },
	    begins_with:     function(v){ o={};o[v[0]]={'$regex': '^' + escapeRegExp(v[1])};return o; },
	    not_begins_with:  function(v){ o={};o[v[0]]={'$regex': '^(?!' + escapeRegExp(v[1]) + ')'};return o; },
	    contains:         function(v){ o={};o[v[0]]={'$regex': escapeRegExp(v[1])};return o; },
	    not_contains:     function(v){ o={};o[v[0]]={'$regex': '^((?!' + escapeRegExp(v[1]) + ').)*$', '$options': 's'};return o; },
	    ends_with:       function(v){ o={};o[v[0]]={'$regex': escapeRegExp(v[1]) + '$'};return o; },
	    not_ends_with:    function(v){ o={};o[v[0]]={'$regex': '(?<!' + escapeRegExp(v[1]) + ')$'};return o; },
	    is_empty:         function(v){ o={};o[v[0]]='';return o; },
	    is_not_empty:     function(v){ o={};o[v[0]]={'$ne': ''};return o; },
	    is_null:          function(v){ o={};o[v[0]]=null;return o; },
	    is_not_null:      function(v){ o={};o[v[0]]={'$ne': null};return o; }
	}

	function or_compile(v,mquery)
	{
		var orpresent = false;
		var orkey = false;
		for(var and_arr in mquery['$and'])
		{
			for(var and_data in mquery['$and'][and_arr])
			{
				if(and_data == '$or')
				{
					orkey = true;
					for(var or_obj in mquery['$and'][and_arr]['$or'])
					{
						or_arr = mquery['$and'][and_arr]['$or'];
						for(var or_i in or_arr)
						{
							cmp_arr = or_arr[or_i];
							vkey = Object.keys(v);
							for(var cmp_data in cmp_arr)
							{
								if(cmp_data == vkey[0] && cmp_arr[cmp_data] != v[vkey])
								{
									mquery['$and'][and_arr]['$or'].push(v);
									orpresent = true;
									break;
								}else if(cmp_data == vkey[0] && cmp_arr[cmp_data] == v[vkey])
								{
									orpresent = true;
									break;
								}else{
									orpresent = false;
								}
							}

							if(orpresent == true){ return mquery; }
						}
					}
				}else{
					orkey = false;
				}
			}	
		}

		if(orpresent == false && orkey == true)
		{
			orobj = {'$or': []};
			orobj['$or'].push(v);
			mquery['$and'].push(orobj);
			return mquery;
		}

		if(orkey == false)
		{
			orobj = {'$or': []};
			orobj['$or'].push(v);
			mquery['$and'].push(orobj);

			return mquery;
		}
	}

	var mcond = {
		and: function(v){
			var mquery = JSON.parse(localStorage['query']);
			if(typeof mquery === 'object' && mquery !== null){
				if('$and' in mquery)
				{
					mquery['$and'].push(v);
					localStorage['query'] = JSON.stringify(mquery);
				}else{
					mquery['$and'] = [v];
					localStorage['query'] = JSON.stringify(mquery);
				}
				
			}else{
				mquery = {'$and': [v]};
				localStorage['query'] = JSON.stringify(mquery);
			}

			console.log(mquery);
			return mquery; 
		},

		or: function(v){ 
			var mquery = JSON.parse(localStorage['query']);
			var orpresent = false;
			if($.isArray(v) && mquery !== null){
				if('$and' in mquery)
				{
					orobj = {'$or': v};
					mquery['$and'].push(orobj);
					localStorage['query'] = JSON.stringify(mquery);
				}else{
					orobj = {'$or': v};
					mquery = {'$and': [orobj]};
					localStorage['query'] = JSON.stringify(mquery);
				}
			}else if(typeof mquery === 'object' && mquery !== null){
				if('$and' in mquery)
				{
					cquery = or_compile(v,mquery);
					localStorage['query'] = JSON.stringify(cquery);
				}else{
					orobj = {'$or': []};
					orobj['$or'].push(v)
					mquery = {'$and': [orobj]};
					localStorage['query'] = JSON.stringify(mquery);
				}
			}else{
				if($.isArray(v))
				{
					orobj = {'$or': v};
					mquery = {'$and': [orobj]};
					localStorage['query'] = JSON.stringify(mquery);
				}else{
					orobj = {'$or': []};
					orobj['$or'].push(v)
					mquery = {'$and': [orobj]};
					localStorage['query'] = JSON.stringify(mquery);
				}
			}

			return mquery;  
		}
	}

	var mopts = []

	$.each(mopers, function(i,v){
		mopts.push(i.toString());
	});

	$(".pheaders").change(function(){
		var ldata = $(this).parent().parent().find(".query-data > .ldata");
		if(ldata.hasClass("editable") != false)
		{
			ldata.editable("destroy");
		}

	});

	$(".moptions").each(function(){
		var optml = "";
		for(var i = 0; i < mopts.length; i++)
		{
			optml += "<option value=\""+mopts[i]+"\">"+mopts[i]+"</option>";
		}

		$(this).append(optml);
	});

	$(document).delegate(".add-query", "click", function(e){
		var qid = parseInt($(this).parent().parent().data('qid')) + 1;
		var qclone = $(this).parent().parent().clone();
		qclone.data('qid', qid.toString());
		qclone.find(".query-controls > .remove-query").attr("disabled", false);
		$(".query-container").append(qclone);
	});

	$(document).delegate(".remove-query", "click", function(e){
		$(this).parent().parent().remove();
	});

	function compile_query(do_search){
		$(".ldata").each(function(){
			var qval = $(this).val();
			var qhead = $(this).parent().parent().find(".query-header > .pheaders").val();
			var qopval = $(this).parent().parent().find(".query-option > .moptions").val();
			var qcon = $(this).parent().parent().find(".query-condition > .qconditions").val();
			var qid = parseInt($(this).parent().parent().data('qid'));

			console.log(qval.length);

			if(qopval == "is_not_null" || qopval == "is_not_empty" || qopval == "is_null" || qopval == 'is_empty')
			{
				cval = [qhead];
				qop_obj = mopers[qopval](cval);
				mcond[qcon](qop_obj);
			}
			else if(qval.length > 0)
			{
				console.log(qhead);
				if(qhead == "location_employee_size_actual" || qhead == "insurance_expenses")
				{
					var qsplit = qval.replace( /\n/g, " " ).split(/[\r\n\,]+/);

					if(qsplit.length > 1)
					{
						for(var i = 0; i < qsplit.length; i++)
						{
							// check if integer or float
							if(Math.round(qsplit[i]) == qsplit[i] && qsplit[i].toString().indexOf(".") != -1)
							{
								cval = [qhead,parseFloat(qsplit[i])];
								qop_obj = mopers[qopval](cval);
								mcond['or'](qop_obj);
							}else if(qsplit[i] % 1 === 0){
								cval = [qhead,parseFloat(qsplit[i])];
								qop_obj = mopers[qopval](cval);
								mcond['or'](qop_obj);
							}
							
						}
					}else{
						cval = [qhead,parseFloat(qval)];
						qop_obj = mopers[qopval](cval);
						mcond[qcon](qop_obj);
					}
					
				}else{
					var qsplit = qval.replace(/^\s+/g,"").split(/[\r\n\,]+/);
					if(qsplit.length > 1)
					{
						for(var i = 0; i < qsplit.length; i++)
						{
							cval = [qhead,$.trim(qsplit[i])];
							qop_obj = mopers[qopval](cval);
							if(qopval == "not_equal"){
								mcond['and'](qop_obj);
								$(this).parent().parent().find(".query-condition > .qconditions").val('and');
							}else{
								mcond['or'](qop_obj);
								$(this).parent().parent().find(".query-condition > .qconditions").val('or');
							}
							
							
						}
					}else{
						cval = [qhead,qval];
						qop_obj = mopers[qopval](cval);
						mcond[qcon](qop_obj);
					}
				}
			}
		});

		console.log(typeof localStorage['zip_rule'], localStorage['zip_rule'].length);

		if(localStorage['zip_rule']){
			var zip_rule = JSON.parse(localStorage['zip_rule']);
			if(zip_rule['$or'].length > 0)
			{
				for(var i = 0; i < zip_rule['$or'].length; i++)
				{
					console.log(zip_rule['$or'][i]);
					mcond['and'](zip_rule['$or'][i]);
					//mcond['or'](zip_rule['$or'][i]);
				}
			}
		}

		console.log(localStorage['county_state_rule'])

		if(localStorage['county_state_rule']){
			var county_state_rule = JSON.parse(localStorage['county_state_rule']);
			if(county_state_rule.length > 0)
			{
				for(var i = 0; i < county_state_rule.length; i++)
				{
					mcond['or'](county_state_rule[i]);
				}
				
			}
		}

		console.log(localStorage['query']);

		if(do_search == true)
		{
			build_search();
		}
	}

	$(".parse-mongo").on('click', function(){
		localStorage['query'] = null;

		$("#snotify").notify(
			"Searching..",
			{ className: "info", autoHide: false }
		);

		compile_query(true);
	});

	// clear search
	function clear_search(){
		$(".query-object").each(function(){
			if($(this).data('qid') != "0")
			{
				$(this).remove();
			}
			
		});
	}

	$(".clear-query").on('click', clear_search);

	$(".save-query").on('click', function(){
		debugger
		bootbox.prompt("Enter name of list", function(result) {
			caller_id = $("#callid").val();
			console.log(caller_id, caller_id.length == 0, caller_id.length != 0);
			console.log(localStorage['query'], localStorage['query'] != "null", localStorage['query'] == "null");
			if (result !== null && localStorage['query'] != "null" && caller_id.length > 0) {
				console.log("result not null, not null ls query, caller id gt 0");
				phone_rgx = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
				if(caller_id.match(phone_rgx) !== null)
				{
					$.ajax({
						url: "{% url 'campaign:campaign-create' %}",
						type: 'post',
						data: {
							'name': result, 
							'search': localStorage['query'],
							'caller_id': caller_id, 
							'type': 'save'
						},
						success: function(data){
							if(data.message){
								if(data.results == "success"){
									bootbox.alert(data.message);
								}
								else if(data.results == "error"){
									$("#snotify").notify(
										data.message,
										{ className: "error", autoHide: false }
									);
								}
							}else{
								$("#snotify").notify(
									"Save failed - please try again",
									{ className: "error", autoHide: false }
								);
							}
						}
					});
				}else{
					bootbox.alert("Please enter a valid caller id. Allowed phone number formats - (123) 456-7890, 123-456-7890, 123.456.7890 & 1234567890");
				}
			}else if(result !== null && localStorage['query'] == "null" && caller_id.length > 0){
				compile_query(false);
				phone_rgx = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
				if(caller_id.match(phone_rgx) !== null)
				{
					$.ajax({
						url: "{% url 'campaign:campaign-create' %}",
						type: 'post',
						data: {
							'name': result, 
							'search': localStorage['query'],
							'caller_id': caller_id,
							'type': 'save'
						},
						success: function(data){
							if(data.message){
								if(data.results == "success"){
									bootbox.alert(data.message);
								}
								else if(data.results == "error"){
									$("#snotify").notify(
										data.message,
										{ className: "error", autoHide: false }
									);
								}
							}else{
								$("#snotify").notify(
									"Save failed - please try again",
									{ className: "error", autoHide: false }
								);
							}
						}
					});
				}else{
					bootbox.alert("Please enter a valid caller id. Allowed phone number formats - (123) 456-7890, 123-456-7890, 123.456.7890 & 1234567890");
				}
			}else if(result !== null && localStorage['query'] != "null" && caller_id.length == 0){ 
				$.ajax({
					url: "{% url 'campaign:campaign-create' %}",
					type: 'post',
					data: {
						'name': result, 
						'search': localStorage['query'],
						'caller_id': '+12672971999', 
						'type': 'save'
					},
					success: function(data){
						if(data.message){
							if(data.results == "success"){
								bootbox.alert(data.message);
							}
							else if(data.results == "error"){
								$("#snotify").notify(
									data.message,
									{ className: "error", autoHide: false }
								);
							}
						}else{
							$("#snotify").notify(
								"Save failed - please try again",
								{ className: "error", autoHide: false }
							);
						}
					}
				});
			}else if(result !== null && localStorage['query'] == "null" && caller_id.length == 0){ 
				compile_query(false);
				$.ajax({
					url: "{% url 'campaign:campaign-create' %}",
					type: 'post',
					data: {
						'name': result, 
						'search': localStorage['query'],
						'caller_id': '+12672971999', 
						'type': 'save'
					},
					success: function(data){
						if(data.message){
							if(data.results == "success"){
								bootbox.alert(data.message);
							}
							else if(data.results == "error"){
								$("#snotify").notify(
									data.message,
									{ className: "error", autoHide: false }
								);
							}
						}else{
							$("#snotify").notify(
								"Save failed - please try again",
								{ className: "error", autoHide: false }
							);
						}
					}
				});
			}else{
				bootbox.alert("Please enter save name and caller id");
			}
		}); 
		
	});

	function mgmt_countystate(state, county, lselected){
		console.log(state, county, lselected);
		if(lselected === undefined || lselected == false)
		{
			if(localStorage['state_rule'])
			{
				var state_rule = JSON.parse(localStorage['state_rule']);
				state_rule["$or"].push({'location_state': state});
				localStorage['state_rule'] = JSON.stringify(state_rule);

			}else{
				console.log("state_rule not found", localStorage['state_rule']);
			}
		}else{
			if(localStorage['state_rule'] && localStorage['county_rule'])
			{
				var state_rule = JSON.parse(localStorage['state_rule']);
				var county_rule = JSON.parse(localStorage['county_rule']);

				console.log(county_rule);
				var fstate = county_rule["$or"].map(
					function(e){ return e['county_description']; }
				).indexOf(county);

				console.log(fstate);

				if(fstate > -1){
					state_rule["$or"].splice(fstate, 1);
				}

				localStorage['state_rule'] = JSON.stringify(state_rule);
				

				//add_query();
			}else{
				console.log("state_rule and county_rule not found", localStorage['county_rule'], localStorage['state_rule']);
			}
		}
	}

	function compilecounty(){
		if(localStorage['state_rule'] && localStorage['county_rule'] && localStorage['county_state_rule'])
		{
			var state_rule = JSON.parse(localStorage['state_rule']);
			var county_rule = JSON.parse(localStorage['county_rule']);
			
			var cs_rule = [];

			
			for(var i = 0; i < county_rule["$or"].length; i++)
			{
				var tobj = {
					"$and":[
						{'county_description': county_rule["$or"][i]['county_description']},
						{'location_state': state_rule["$or"][i]['location_state']}
					]
				}

				cs_rule.push(tobj);
			}

			localStorage['county_state_rule'] = JSON.stringify(cs_rule);

			total_json = cs_rule.length;
			
			$("#map_results_county > #county_cnt").text(total_json.toString());
			$("#map_results_county > #county_list").text(JSON.stringify(cs_rule));

		}else{ console.log("state_rule, county_rule and county_state_rule not found", localStorage['county_rule'], localStorage['state_rule'], localStorage['county_state_rule']); }
	}

	function selectcounty(e){
		//county_description
		var layer = e.layer;
		console.log("===============================", layer.selected)
		console.log(layer);
		debugger
		var cstate = FIPS_STATE[layer.feature.properties.STATE];
		var county_list = [];

		if(layer.feature.properties.LSAD == "city")
		{
			var cname = layer.feature.properties.NAME + " City";
		}else{
			var cname = layer.feature.properties.NAME;
		}

		mgmt_countystate(cstate, cname, layer.selected);
		console.log(layer.feature.properties);

		if(layer.selected === undefined || layer.selected == false)
		{
			layer.setStyle({
				fillColor: "#ff6666",
				fillOpacity: 0.5
			});

			layer.selected = true;

			if(localStorage['county_rule'])
			{
				var county_rule = JSON.parse(localStorage['county_rule']);
				
				if(localStorage['county_list'] && localStorage['county_list'].length > 0)
				{
					var clist = localStorage['county_list'].split(",");

					var fcounty = county_rule["$or"].map(
					function(e){ return e['county_description']; }
					).indexOf(cname);

					if(fcounty <= -1){
						county_rule["$or"].push({'county_description': cname});
						$.each(county_rule['$or'], function(i,v){
							if(clist.indexOf(v['county_description']) <= -1)
							{
								clist.push(v['county_description']);
							}
						});

						localStorage['county_list'] = clist;
					}
				}else{
					var clist = [];

					county_rule["$or"].push({'county_description': cname});
					total_json = county_rule['$or'].length;
					$.each(county_rule['$or'], function(i,v){
						if(clist.indexOf(v['county_description']) <= -1)
						{
							clist.push(v['county_description']);
						}
					});

					localStorage['county_list'] = clist;
				}

				localStorage['county_rule'] = JSON.stringify(county_rule);

				compilecounty();

			}else{
				console.log("county_rule not found", localStorage['county_rule']);
			}
		}else{
			layer.setStyle({
				fillColor: "#FFFFFF",
				fillOpacity: 0.1
			});

			layer.selected = false;
			
			if(localStorage['county_rule'])
			{
				var county_rule = JSON.parse(localStorage['county_rule']);
				var clist = localStorage['county_list'].split(",");

				var fcounty = county_rule["$or"].map(
					function(e){ return e['county_description']; }
				).indexOf(cname);

				if(fcounty > -1){
					county_rule["$or"].splice(fcounty, 1);
					total_json = county_rule['$or'].length;
					if(clist && clist.length > 0)
					{
						if(clist.indexOf(cname) > -1)
						{
							clist.splice(clist.indexOf(cname), 1);
						}

						localStorage['county_list'] = clist;
					}
				}

				localStorage['county_rule'] = JSON.stringify(county_rule);

				compilecounty();

			}else{
				console.log("county_rule not found", localStorage['county_rule']);
			}

			console.log(localStorage['county_rule']);
		}
	}

	function draw_created(e){
		console.log(e.layer);

		if($.isFunction(e.layer.getRadius))
		{
			var mradius_miles = e.layer.getRadius()*0.000621371;
			var mradius_rads = (e.layer.getRadius()*0.001)/6371;
			var mcenter = [e.layer.getLatLng().lng, e.layer.getLatLng().lat];

			//bound_search(e.layer.getLatLng(), map.getBounds(), e.layer.getRadius(), "add", map);
			featureGroup.addLayer(e.layer);
			console.log(e.layers);

			if(localStorage['zip_rule'])
			{
				var zip_rule = JSON.parse(localStorage['zip_rule']);
				var found = false;

				for(var i = 0; i < zip_rule["$or"].length; i++){
					for(var j = 0; j < zip_rule["$or"][i]["loc"]["$geoWithin"]["$centerSphere"].length; j++)
					{
						var csobj = zip_rule["$or"][i]["loc"]["$geoWithin"]["$centerSphere"][i];
						if($.isArray(csobj) && csobj.equals(mcenter))
						{
							found = true;
							break;
						}
					}
				}

				if(found == false){
					var ngeoq = {
						"loc": {
							"$geoWithin": {
								"$centerSphere": [
									mcenter,
									mradius_rads
								]
							}
						}
					}
					zip_rule["$or"].push(ngeoq);

				}

				localStorage['zip_rule'] = JSON.stringify(zip_rule);
			}
		} else {
			var layer = e.layer;

			featureGroup.addLayer(layer);

			var shape = layer.toGeoJSON();

			var coords = JSON.stringify(shape.geometry.coordinates[0]);
			var shape_for_db = JSON.stringify(shape);

			console.log((LGeo.area(e.layer) / 1000000).toFixed(2));
			console.log(LGeo.area(e.layer));
			console.log(shape);
			console.log(coords);
			console.log(shape_for_db);
		}
		
	}

	function draw_deleted(e){
		var layers = e.layers;
		if(localStorage['zip_rule'])
		{
			var zip_rule = JSON.parse(localStorage['zip_rule']);
			layers.eachLayer(function(layer){
				for(var i = 0; i < zip_rule["$or"].length; i++)
				{
					for(var j = 0; j < zip_rule["$or"][i]["loc"]["$geoWithin"]["$centerSphere"].length; j++)
					{
						var csobj = zip_rule["$or"][i]["loc"]["$geoWithin"]["$centerSphere"][i];
						if($.isArray(csobj) && csobj.equals([layer.getLatLng().lng, layer.getLatLng().lat]))
						{
							found = true;
							zip_rule["$or"].splice(i,1);
							break;
						}
					}
				}
			});

			localStorage['zip_rule'] = JSON.stringify(zip_rule);
		}
		
	}

	function build_search(){
		$.ajax({
			url: "{% url 'campaign:campaign-create' %}",
			type: 'post',
			data: { 
				'search': localStorage['query'],
				'type': 'search' 
			},
			success: function(data){
				if(data.message){
					if(data.results == "error"){
						$("#snotify").notify(
							data.message,
							{ className: "error", autoHide: false }
						);
					}else if(data.results == "success"){
						$("#snotify").notify(
							data.message,
							{ className: "success", autoHide: false }
						);
					}else{
						$("#snotify").notify(
							"no result info passed - " + data.message,
							{ className: "error", autoHide: false }
						);
					}
				}

				if(data.prospects.length > 0 && data.prospects_field.length > 0){
					var client_mdata = [];
					var id_pos = 0;

					// build headers for data table
					for(var i = 0; i < data.prospects_field.length; i++){
						client_mdata.push({
							"mDataProp": String(data.prospects_field[i]), 
							"sTitle": String(data.prospects_field[i]), 
							"sTargets": [i],
							"sType": "string"
						});

					}


					// send to table
					$('#results').dataTable({
						destroy: true,
						"aaData": data.prospects,
						"aoColumns": client_mdata
					});

				}
			}
		});
	}


	// Map Initialization
	L.mapbox.accessToken = 'pk.eyJ1IjoibGVhZG9yY2hhcmQiLCJhIjoiZExQWEk3MCJ9.lpnxsmIAyyoDSBo0-1QfVw';
	var map = L.mapbox.map('boxmap', 'leadorchard.k4ghnihh', { minZoom: 7 }).setView([39.971083, -75.172278], 9);
	var countyLayer = L.mapbox.featureLayer()
		.loadURL('/static/geojson/gz_2010_us_050_00_5m.json')
		.on('ready', function(ldata){
			var layer = ldata.target;
			var layers = ldata.target._layers;
			layer.setStyle({
				weight: 3,
				fillColor: "#FFFFFF",
				fillOpacity: 0.1,
				color: "#555555"
			});
		})
		.on('click', selectcounty)
		.addTo(map);
	var featureGroup = L.featureGroup().addTo(map);
	var geocoder = L.mapbox.geocoder('mapbox.places');
	var geocoderControl = L.mapbox.geocoderControl('mapbox.places').addTo(map);
	var drawControl = new L.Control.Draw({
		draw: {
			polyline: false,
			polygon: {
				shapeOptions: {
					color: "#FF4719",
					dashArray: null,
					fill: true,
					fillColor: "#FF6B46",
					fillOpacity: 0.4,
					lineCap: null,
					lineJoin: null,
					noClip: false,
					opacity: 0.7,
					smoothFactor: 1,
					stroke: true,
					weight:4
				}
			},
			rectangle: false,
			marker: false,
			circle: {
				shapeOptions: {
					stroke: true,
					color: '#007f00',
					weight: 4,
					opacity: 0.5,
					fill: true,
					fillColor: null, //same as color by default
					fillOpacity: 0.2,
					clickable: true
				},
				showRadius: true,
				metric: false // Whether to use the metric meaurement system or imperial
			}
		},

		edit: {
			featureGroup: featureGroup,
			edit: false
		}
	}).addTo(map);

	map.on('draw:created', draw_created);

	map.on('draw:deleted', draw_deleted);

	geocoderControl.on('found', function(res){
		L.mapbox.featureLayer({
			type: 'Feature',
			geometry: res.results.features[0].geometry,
			properties: {
				title: res.results.features[0].id,
				description: res.results.features[0].place_name,
				'marker-color': '#BE9A6B',
				'marker-symbol': 'commercial'
			}
		}).addTo(map);
	});

});