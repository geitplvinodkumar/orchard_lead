$(document).ready(function () {
      $( ".datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "1900:2012",
          autoclose: true,
          // You can put more options here.
      });
      $('.timePicker').datetimepicker({
          defaultDate: new Date(),
          changeMonth: true,
          changeYear: true,
          yearRange: "1900:2012", 
          format:'YYYY-MM-DD HH:mm:ss',
          autoclose: true,
      });
});