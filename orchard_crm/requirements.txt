Django==1.10.1
Pillow==3.3.1
psycopg2==2.6.2
python-memcached==1.58
six==1.10.0
Unipath==1.1
