from django.shortcuts import render
from django.views.generic import ListView, CreateView
from appointment.models import Appointment
from .forms import *
from lead.forms import *
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, Http404, HttpResponse 


class AppointmentListView(ListView):
    """docstring for AppointmentListView"""
    model = Appointment
    template_name = 'appointment/appointment_list.html'
    context_object_name = 'appointment_list'
    paginate_by = 10



def create(request):
      if request.method == 'POST':
            form = AppointmentCreateForm(data=request.POST)
            if form.is_valid():
                form_obj = form.save(commit=False)
                lead = Lead.objects.create(
                  first_name=form_obj.first_name, last_name=form_obj.last_name, company_name=form_obj.company_name,
                  phone_number=form_obj.phone_number, address=form_obj.address, city=form_obj.city,state=form_obj.state,
                  zip_code=form_obj.zip_code, appointment_datetime=form_obj.appointment_datetime, agency_calling_for=form_obj.agency_calling_for,
                  appointment_type=form_obj.appointment_type, lead_comments=form_obj.appointment_comment,
                  )
                lead.save()
                form_obj.lead_id = lead.id 
                form_obj.save()
                return HttpResponseRedirect(reverse("appointment:appointment-list"))
      else:
        return render(request, 'appointment/create_appointment.html', {'form':AppointmentCreateForm})
     

class AppointmentCreateView(CreateView):
      model = Appointment
      form = AppointmentCreateForm
      template_name = 'appointment/create_appointment.html'