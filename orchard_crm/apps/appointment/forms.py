from django.utils.translation import ugettext_lazy as _
from django import forms
from appointment.models import Appointment
class AppointmentCreateForm(forms.ModelForm):
  class Meta:
    model = Appointment
    exclude = ('title', 'county', 'agent','lead',)
    widgets = {
          'first_name': forms.TextInput(attrs={'type':'first_name', 'class':'form-control'}),
          'last_name': forms.TextInput(attrs={'type':'last_name', 'class':'form-control'}),
          'phone_number': forms.TextInput(attrs={'type':'phone_number', 'class':'form-control'}),
          'company_name': forms.TextInput(attrs={'type':'company_name', 'class':'form-control'}),
          'address': forms.TextInput(attrs={'type':'address', 'class':'form-control'}),
          'city': forms.TextInput(attrs={'type':'location_city', 'class':'form-control'}),
          'state': forms.TextInput(attrs={'type':'location_state', 'class':'form-control'}),
          'zip_code': forms.TextInput(attrs={'type':'location_zip_code', 'class':'form-control'}),
          'agency_calling_for': forms.TextInput(attrs={'type':'agency_calling_for', 'class':'form-control'}),
          'appointment_datetime': forms.DateInput(attrs={'type':'appointment_datetime', 'class':'timePicker'}),
          'appointment_type': forms.TextInput(attrs={'type':'appointment_type', 'class':'form-control'}),
          'appointment_status': forms.TextInput(attrs={'type':'appointment_status', 'class':'form-control'}),
          'appointment_comment': forms.TextInput(attrs={'type':'appointment_comment', 'class':'form-control'}),
    }