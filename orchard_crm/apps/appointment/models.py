from django.db import models
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.conf import settings
from prospect.models import *
from lead.models import *
from contact.models import *
from django.core.urlresolvers import reverse, reverse_lazy
# Create your models here.

class Appointment(AbstractCommon, models.Model):
    """
    Description: Model Description
    """
    appointment_datetime = models.DateTimeField(null=True)
    agency_calling_for = models.TextField(null=True)
    agent = models.ForeignKey(Contact, null=True, related_name='appointment_agent')
    appointment_type = models.TextField(null=True)
    appointment_status = models.TextField(null=True)
    appointment_comment = models.TextField(null=True)
    lead = models.ForeignKey(Lead, null=True, related_name='appointment_lead')
    def __unicode__(self):
        return self.agent
    def get_absolute_url(self):
        return reverse("appointment:appointment-list")
