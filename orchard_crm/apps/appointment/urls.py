from django.conf.urls import url
from appointment.views import *

urlpatterns = [
    url(r'^$', AppointmentListView.as_view(), name='appointment-list'),
    url(r'^create$', create, name='create-appointment'),
]