from django.db import models

# Create your models here.
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.conf import settings
# Create your models here.

class AbstractCommon(models.Model):
    """
    Description: Model Description
    """
    company_name = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    state = models.CharField(max_length=255, null=True, blank=True)
    zip_code = models.CharField(max_length=255, null=True, blank=True)
    county = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        abstract = True 

class Prospect(AbstractCommon, models.Model):
    """
    Description: Model Description
    """
    call_result  = models.CharField(max_length=255)
    call_result_description = models.CharField(max_length=255)
    renewal_date = models.DateField(max_length=255)
    email = models.EmailField(max_length=255)
    comments = models.CharField(max_length=255)
    sic_code = models.CharField(max_length=255)
    sic_code_description = models.CharField(max_length=255)
    naics_code = models.CharField(max_length=255)
    naics_code_description = models.CharField(max_length=255)
    employee_size = models.CharField(max_length=255)
    website = models.CharField(max_length=255)
    square_footage = models.CharField(max_length=255)
    location = models.PointField(max_length=255, null=True, blank=True)
    credit_rating = models.CharField(max_length=255)
    last_appointment_with = models.ForeignKey(settings.AUTH_USER_MODEL)
    alternative_phone_number = models.CharField(max_length=255)
    carrier_of_record = models.CharField(max_length=255)
    lcm = models.CharField(max_length=255)