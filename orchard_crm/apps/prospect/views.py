from django.shortcuts import render
from django.views.generic import ListView


# Create your views here.
class ProspectListView(ListView):
	"""docstring for ProspectListView"""
	def __init__(self, arg):
		super(ProspectListView, self).__init__()
		self.arg = arg
		