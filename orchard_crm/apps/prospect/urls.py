from django.conf.urls import url
from prospect.views import *

urlpatterns = [
    url(r'', ProspectListView.as_view(), name='prospect-list'),
]