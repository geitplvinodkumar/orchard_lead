from django.conf.urls import url
from contact.views import *

urlpatterns = [
    url(r'', ContactListView.as_view(), name='contact-list'),
]