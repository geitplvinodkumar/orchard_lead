from django.shortcuts import render
from django.views.generic import ListView


# Create your views here.
class ContactListView(ListView):
	"""docstring for ContactListView"""
	def __init__(self, arg):
		super(ContactListView, self).__init__()
		self.arg = arg
		