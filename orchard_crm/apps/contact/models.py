from django.db import models
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.conf import settings
from prospect.models import *
from contact.models import *
# Create your models here.

class Contact(AbstractCommon, models.Model):
    """
    Description: Model Description
    """
    email = models.EmailField(max_length=255)
    additional_email = models.EmailField(max_length=255)
    password = models.CharField(max_length=50)
    log_in_type = models.CharField(max_length=255)
    account_name = models.CharField(max_length=255)
    agency_name = models.CharField(max_length=255)
    notes = models.CharField(max_length=255)
    plan_purchased = models.CharField(max_length=255)
    number_of_appointment = models.IntegerField()
    is_client_Active = models.BooleanField()
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, unique=False, related_name='contact_created_by', on_delete=models.CASCADE)



