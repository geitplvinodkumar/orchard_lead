from django.shortcuts import render
from django.views.generic import ListView
from users.models import *
from django.core.cache import cache 
import datetime
from django.conf import settings
from django.core.cache import cache
from django.conf import settings

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Create your views here.
class HomeDashboard(ListView):
    """docstring for HomeDashboard"""
    template_name = 'home/dashbord.html'
    model = CustomUser
    context_object_name = 'users'

    