from django.conf.urls import url
from home.views import *
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', login_required(HomeDashboard.as_view()), name='home-dashboard'),
]