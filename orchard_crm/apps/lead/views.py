from django.shortcuts import render, render_to_response, get_list_or_404, get_object_or_404
from django.views.generic import ListView, UpdateView, DetailView, DeleteView
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect, Http404, HttpResponse
from .models import *
from .forms import *
from django.core.urlresolvers import reverse, reverse_lazy 


class LeadListView(ListView):
    """docstring for LeadListView"""
    model = Lead
    template_name = 'lead/lead_list.html'
    context_object_name = 'lead_list'
    paginate_by = 10

class LeadCreateView(CreateView):
    model = Lead
    form_class = LeadCreateForm
    template_name = 'lead/create_lead.html'
    def create(request):
        if request.method == 'POST':
            form = form_class(data=request.POST, files=request.FILES)
            if form.is_valid():
                # form.instance.created_by = self.request.user

                form.save()
                return HttpResponseRedirect(reverse("lead:lead-list"))
        else:
            return render(request, self.template_name, {'form':LeadCreateForm})            



class LeadUpdateView(UpdateView):
    model = Lead
    form_class = LeadCreateForm
    template_name = 'lead/lead_update.html'
# import pdb; pdb.set_trace()
class LeadDeleteView(DeleteView):
    template_name = 'lead/lead_delete.html'
    model = Lead
    success_url = reverse_lazy('lead:lead-list')