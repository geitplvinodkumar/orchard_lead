from django.db import models
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse
from prospect.models import *
from contact.models import *
# from appointment.models import Appointment
# Create your models here.

class Lead(AbstractCommon, models.Model):
  appointment_datetime = models.DateTimeField(null=True)
  company_name = models.TextField(null=True)
  title = models.TextField(null=True, blank=True)
  first_name = models.TextField(null=True,max_length=30)
  last_name = models.TextField(null=True,max_length=30)
  phone_number = models.TextField(null=True,max_length=10)
  alternative_phone_number = models.TextField(null=True, blank=True,max_length=10)
  email_address = models.EmailField(null=True, blank=True)
  company_website = models.TextField(null=True, blank=True)
  years_in_business = models.TextField(null=True, blank=True)
  agency_calling_for = models.TextField(null=True)
  agent = models.ForeignKey(Contact, null=True, related_name='lead_agent')
  appointment_type = models.TextField(null=True)
  verified = models.BooleanField(default=False)

  address = models.TextField(null=True)
  city = models.TextField(null=True)
  state = models.TextField(null=True)
  zip_code = models.TextField(null=True)
  county = models.TextField(null=True, blank=True)
  location_latitude_longitude = models.TextField(null=True, blank=True)

  sic_code = models.TextField(null=True, blank=True)
  sic_code_description = models.TextField(null=True, blank=True)
  naics_code = models.TextField(null=True, blank=True)
  naics_code_description = models.TextField(null=True, blank=True)

  lead_category= models.TextField(null=True, blank=True) 
  lead_created_by = models.TextField(null=True, blank=True) 
  lead_created = models.DateTimeField(auto_now_add=True) 
  lead_modified_by = models.TextField(null=True, blank=True) 
  lead_modified = models.DateTimeField(auto_now_add=True) 
  lead_category_modified_by = models.TextField(null=True, blank=True) 
  lead_category_modified = models.DateTimeField(auto_now_add=True) 
  
  next_renewal_date = models.DateField(null=True, blank=True, auto_now_add=True)
  liability_carrier = models.TextField( blank=True)
  liability_renewal_date = models.DateField(null=True, blank=True, auto_now_add=True)
  total_locations = models.TextField( blank=True)
  workers_compensation_renewal_date = models.DateField(null=True, blank=True, auto_now_add=True)
  workers_compensation_carrier = models.TextField( blank=True)
  total_employees = models.IntegerField(default=0, null=True, blank=True)
  auto_renewal_date = models.DateField(null=True, blank=True,auto_now_add=True)
  auto_carrier = models.TextField(blank=True)
  total_vehicles = models.IntegerField(default=0)

  lead_comments = models.TextField(null=True)
  user_notes = models.TextField(null=True, blank=True)
  attachments = models.FileField(null=True, upload_to='documents/%Y/%m/%d', blank=True)
  return_for_credit_request = models.TextField(null=True, blank=True)
  # appointment = models.ForeignKey(to=Appointment, related_name='lead_appointment', null=True, blank=True)
  def __unicode__(self):
        return self.lead_created_by
  def get_absolute_url(self):
        return reverse("lead:lead-list")
  # def __unicode__(self):
  #   return "[%s] [created]: %s [updated]: %s" % (str(self.lead_owner.first_name)+" "+str(self.lead_owner.last_name),str(self.created),str(self.modified))