from django.conf.urls import url, include
from django.views.generic import View
from lead.views import *

urlpatterns = [
    url(r'^$', LeadListView.as_view(), name='lead-list'),
    url(r'^create$', LeadCreateView.as_view(), name='create'),
    url(r'^update/(?P<pk>\d+)/$', LeadUpdateView.as_view(), name='update'),
    url(r'^delete/(?P<pk>\d+)/$', LeadDeleteView.as_view(), name='delete'),
]