from django.utils.translation import ugettext_lazy as _
from django import forms
from .models import Lead
class LeadCreateForm(forms.ModelForm):
  class Meta:
    model = Lead
    exclude = ('lead_created','lead_modified','lead_category_modified','agent',)
    
    def __init__(self, *args, **kwargs):
      super(LeadCreateForm, self).__init__(*args, **kwargs)
    widgets = {
          'appointment_datetime': forms.DateInput(attrs={'type':'appointment_datetime', 'class':'timePicker'}),
          'company_name': forms.TextInput(attrs={'type':'company_name', 'class':'form-control'}),
          'title': forms.TextInput(attrs={'type':'title', 'class':'form-control'}),
          'first_name': forms.TextInput(attrs={'type':'first_name', 'class':'form-control'}),
          'last_name': forms.TextInput(attrs={'type':'last_name', 'class':'form-control'}),
          'phone_number': forms.TextInput(attrs={'type':'phone_number', 'class':'form-control'}),
          'alternative_phone_number': forms.TextInput(attrs={'type':'alternative_phone_number', 'class':'form-control'}),
          'email_address': forms.EmailInput(attrs={'type':'email_address', 'class':'form-control'}),
          'company_website': forms. URLInput(attrs={'type':'company_website', 'class':'form-control'}),
          'years_in_business': forms.TextInput(attrs={'type':'years_in_business', 'class':'form-control'}),
          'agency_calling_for': forms.TextInput(attrs={'type':'agency_calling_for', 'class':'form-control'}),
          'appointment_type': forms.TextInput(attrs={'type':'appointment_type', 'class':'form-control'}),
          'address': forms.TextInput(attrs={'type':'address', 'class':'form-control'}),
          'city': forms.TextInput(attrs={'type':'location_city', 'class':'form-control'}),
          'state': forms.TextInput(attrs={'type':'location_state', 'class':'form-control'}),
          'zip_code': forms.TextInput(attrs={'type':'location_zip_code', 'class':'form-control'}),
          'county': forms.TextInput(attrs={'type':'county', 'class':'form-control'}),
          'location_latitude_longitude': forms.TextInput(attrs={'type':'location_latitude_longitude', 'class':'form-control'}),
          'sic_code': forms.TextInput(attrs={'type':'sic_code', 'class':'form-control'}),
          'sic_code_description': forms.TextInput(attrs={'type':'sic_code_description', 'class':'form-control'}),
          'naics_code': forms.TextInput(attrs={'type':'naics_code', 'class':'form-control'}),
          'naics_code_description': forms.TextInput(attrs={'type':'naics_code_description', 'class':'form-control'}),
          'lead_category': forms.TextInput(attrs={'type':'lead_category', 'class':'form-control'}),
          'lead_created_by': forms.TextInput(attrs={'type':'lead_created_by', 'class':'form-control'}),
          'lead_modified_by': forms.TextInput(attrs={'type':'lead_modified_by', 'class':'form-control'}),
          'lead_category_modified_by': forms.TextInput(attrs={'type':'lead_category_modified_by', 'class':'form-control'}),
          'next_renewal_date': forms.DateInput(attrs={'type':'next_renewal_date', 'class':'datepicker'}),
          'liability_carrier': forms.TextInput(attrs={'type':'liability_carrier', 'class':'form-control'}),
          'liability_renewal_date': forms.DateInput(attrs={'type':'liability_renewal_date', 'class':'datepicker'}),
          'total_locations': forms.TextInput(attrs={'type':'total_locations', 'class':'form-control'}),
          'workers_compensation_renewal_date': forms.DateInput(attrs={'type':'workers_compensation_renewal_date', 'class':'datepicker'}),
          'workers_compensation_carrier': forms.TextInput(attrs={'type':'workers_compensation_carrier', 'class':'form-control'}),
          'total_employees': forms.TextInput(attrs={'type':'total_employees', 'class':'form-control'}),
          'auto_renewal_date': forms.DateInput(attrs={'type':'auto_renewal_date', 'class':'datepicker'}),
          'auto_carrier': forms.TextInput(attrs={'type':'auto_carrier', 'class':'form-control'}),
          'lead_comments': forms.TextInput(attrs={'type':'lead_comments', 'class':'form-control'}),
          'user_notes': forms.TextInput(attrs={'type':'user_notes', 'class':'form-control'}),
          'return_for_credit_request': forms.TextInput(attrs={'type':'return_for_credit_request', 'class':'form-control'}),
    }

    # def __init__(self, *args, **kwargs):
    #     super(LeadCreateForm, self).__init__(*args, **kwargs)

    #     for fields in self.fields:
    #       self.fields[fields].widget = forms.TextInput(attrs={
    #         'class': 'form-control'})
