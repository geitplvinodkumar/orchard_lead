# -*- coding: utf-8 -*-
import time
import uuid
import base64

# Django imports
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from django.conf import settings

from datetime import datetime, date,timedelta
from django.utils.translation import ugettext_lazy as _

class CustomUserManager(BaseUserManager):
    """ Well.. using BaseUserManager """

    def create_user(self, email, password):
        if not email:
            raise ValueError("Users must register an email")

        user = self.model(email=CustomUserManager.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


RELEVANCE_CHOICES = (
    (1, _("Unread")),
    (2, _("Read"))
)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """ Using email instead of username """
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(verbose_name="Staff", default=False)
    bio = models.TextField(blank=True, null=True, verbose_name=u"Bio / About")
    profile_pic = models.ImageField(upload_to="summoners/%s/" % time.time(),
                                    blank=True, null=True)

    # Verification code
    verification_code = models.CharField(max_length=300, blank=True, null=True)
    is_verified = models.BooleanField(default=False)
    # Timestamps
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    first_name = models.CharField(max_length=140,
                                    blank=True, null=True)
    last_name = models.CharField(max_length=140,
                                    blank=True, null=True)
    phone_number = models.CharField(max_length=140,
                                    blank=True, null=True)
    address = models.CharField(max_length=140,
                                    blank=True, null=True)
    city = models.CharField(max_length=140,
                                    blank=True, null=True)
    state = models.CharField(max_length=140, choices = RELEVANCE_CHOICES, default='')
    zip_code = models.CharField(max_length=140,
                                    blank=True, null=True)
    google_calendar_id = models.CharField(max_length=140,
                                    blank=True, null=True)



    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()




    def generate_verification_code(self):
        varification_code = base64.urlsafe_b64encode(uuid.uuid1().bytes.rstrip())[:25]
        return varification_code.decode("utf-8")
        # return base64.urlsafe_b64encode(uuid.uuid1().bytes.encode("base64").rstrip())[:25]

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    @property
    def username(self):
        return self.email

    def get_activation_url(self,request):
        url = u"%s?q=%s" % (request.build_absolute_uri(reverse("user:activate", kwargs={"pk": self.pk})), self.verification_code)
        return url

    def send_activation_email(self, request,password=False):
        """ Send an activation email. Upon clicking said link, activate the model """

        url = u"%s?q=%s" % (request.build_absolute_uri(reverse("user:activate", kwargs={"pk": self.pk})), self.verification_code)
        subject = "Need to activate account"
        body = "you have register with  Orchade ERM please activate account : %s" % url
        send_mail(subject, body, settings.DEFAULT_EMAIL_FROM, [self.email,], fail_silently=False)

    def send_password_reset_email(self, request):
        url = u"%s?q=%s" % (request.build_absolute_uri(reverse("user:reset_password", kwargs={"pk": self.pk})), self.verification_code)
        subject = "password reset"
        body = "Reset password link: %s" % url
        send_mail(subject, body, "admin@domian.com", [self.email,], fail_silently=True)

    def reset_verification_code(self):
        """
        If for any reason we need a new code
        """
        self.verification_code = self.generate_verification_code()
        self.save()

    def save(self, *args, **kwargs):
        """
        If this is a new user, generate code.
        Otherwise leave as is
        """
        if not self.pk:
            self.verification_code = self.generate_verification_code()
        elif not self.verification_code:
            self.verification_code = self.generate_verification_code()

        user = super(CustomUser, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.email