from django.contrib import admin
from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import CustomUser
from .forms import CustomUserCreationForm, CustomUserChangeForm


class CustomUserAdmin(UserAdmin):
    """
    Well.. now we add it to the user admin panel
    """
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    readonly_fields = ("created", "updated")

    list_display = ("email",'created','is_active','is_verified')
    list_filter = ("is_staff", "is_superuser", "is_active", "groups")
    search_fields = ("email", )
    ordering = ('created',"email",)
    filter_horizontal = ("groups", "user_permissions", )
    fieldsets = (
        (None, {"fields": ("email", "password", )}),
        ("Permissions", {"fields": ("is_active",
                "is_staff", 
                "is_superuser",
                "is_verified",
                "verification_code",
                "groups", 
                "user_permissions",
            )}),
        ("Personal information", {"fields": ('profile_pic',)}),
        ("Important dates", {"fields": ("last_login",'updated') }),
    )

    add_fieldsets = (
        (None, {
            "classes": ("wide", ),
            "fields": ("email", "password1", "password2",),
        }),
    )


admin.site.register(CustomUser, CustomUserAdmin)