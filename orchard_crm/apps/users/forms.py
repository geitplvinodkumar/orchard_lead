# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.translation import ugettext_lazy as _
# AGN imports
from .models import CustomUser


class CustomUserCreationForm(forms.ModelForm):
    """Create a new user and stuff"""
    name = forms.CharField(max_length = 100, label = "", required = True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Name'}))
    email = forms.EmailField(max_length = 100, label = "", required = True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Email'}))
    password1 = forms.CharField(label = "", widget = forms.PasswordInput(attrs = {'class':'form-control', 'placeholder': 'Password'}))
    password2 = forms.CharField(label = "", widget=forms.PasswordInput(attrs = {'class':'form-control', 'placeholder': 'Confirm Password'}))
    # is_agree = forms.BooleanField( required = False, label='Agree the terms and policy' )

    class Meta:
        model = CustomUser
        fields = ("email", )
        widgets = {
            'email': forms.TextInput(attrs={'type':'email', 'class':'form-control'}),
        }


    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        # Check if the same
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas deben ser iguales")

        return password2

    def save(self, commit=True):
        user = super(CustomUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password2"])
        if commit:
            user.save()
        return user


class UserProfileUpdateForm(forms.ModelForm):
    summoner_name = forms.CharField(max_length=140, required=True)
    class Meta:
        model = CustomUser
        fields = ("bio", "profile_pic", )
        widgets = {
            'profile_pic': forms.FileInput(attrs={'class': "form_image_field"}),
        }
    def __init__(self, *args, **kwargs):
        super(UserProfileUpdateForm, self).__init__(*args, **kwargs)
        self.fields['bio'].widget.attrs['class'] = "form-control"
        self.fields['bio'].widget.attrs['rows'] = 2

class PasswordResetEmailForm(forms.Form):
    email = forms.EmailField(max_length=200, label="Email",widget=forms.TextInput(attrs={'required':'','type':'email','class':'form-control'}))

class PasswordRecoveryForm(forms.Form):
    activation_key = forms.CharField(widget=forms.HiddenInput, required=False)
    password1 = forms.CharField(max_length=140, 
        label=u"Nueva contraseña", widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Passwords'}), required=False)
    password2 = forms.CharField(max_length=140, 
        label=u"Confirmar contraseña", widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Confirm Password'}), required=False)

    def clean_password2(self):
        password1 = self.cleaned_data["password1"]
        password2 = self.cleaned_data["password2"]
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    class Meta:
        model = CustomUser
        exclude = ['activation_key']
            

class CustomUserChangeForm(forms.ModelForm):
    """Change the users and stuff"""
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        exclude=()

    def clean_password(self):
        return self.initial["password"]

class UserLoginForm(forms.Form):
    username = forms.EmailField(max_length=100, label="", required=True, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder': 'Email'}))
    password = forms.CharField(label="", required=True, widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder': 'Password'}))

class ChnagePassword(forms.Form):

    oldpassword = forms.CharField(max_length=140,
        label=u"Contraseña Actual", widget=forms.PasswordInput(attrs={'class':'form-control'}), required=True)
    password1 = forms.CharField(max_length=140, 
        label=u"Nueva Contraseña", widget=forms.PasswordInput(attrs={'class':'form-control'}), required=True)
    password2 = forms.CharField(max_length=140, 
        label=u"Confirmar Contraseña", widget=forms.PasswordInput(attrs={'class':'form-control'}), required=True)

    def clean_password2(self):
        password1 = self.cleaned_data["password1"]
        password2 = self.cleaned_data["password2"]
        if password1 and password2 and password1 == password2:
            return password2
        raise forms.ValidationError("Las contraseñas deben ser iguales")
    def clean_oldpassword(self):
        oldpassword = self.cleaned_data["oldpassword"]
        if not oldpassword:
            raise forms.ValidationError("debe introducir su contraseña actual")
        return oldpassword

    def check_password(self,user):
        is_password = user.check_password(self.cleaned_data["oldpassword"])
        #raise forms.ValidationError("password nt match")
        return is_password and True or False

RELEVANCE_CHOICES = (
    (1, _("Unread")),
    (2, _("Read"))
)

class UserProfileForm(forms.Form):
    """docstring for UserProfileForm"""
    first_name = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'First Name'}))
    last_name = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Last Name'}))
    email = forms.EmailField(required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Email'}))
    phone_number = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Phone Number'}))
    address = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Address'}))
    city = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'City'}))
    state = forms.ChoiceField(choices = RELEVANCE_CHOICES, required=True, widget = forms.Select(attrs = {'class':'form-control'}))
    zip_code = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Zip Code'}))
    google_calendar_id = forms.CharField(max_length=140, required=True, widget = forms.TextInput(attrs = {'class':'form-control', 'placeholder': 'Google Calendar ID'}))
    