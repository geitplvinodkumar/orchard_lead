from django.conf.urls import url
from django.views.generic import TemplateView
from users.views import *

urlpatterns = [
    url(r'^logout', log_out, name="logout"),
    url(r'^profile', profile, name="profile"),
    url(r'^profile/(?P<pk>\d+)/$', UpdateUserProfileView.as_view(), name="update-profile"),
    url(r'^activate/(?P<pk>\d+)/$', activate, name="activate"),
    url(r'^create/$', create_user, name="create_user"),
    url(r'^request_password_reset/$', request_password_reset, name="request_password_reset"),
    url(r'^reset_password/(?P<pk>\d+)/$',reset_password, name="reset_password"),
    url(r'^login/$', login, name="login"),
    url(r'^changepassword/$', changepassword, name="changepassword"),
]