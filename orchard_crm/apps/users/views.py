# -*- coding: utf-8 -*-
import uuid
import json
from itertools import chain
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.contrib.auth import get_user_model,authenticate, login as user_login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings
from django.db.models import Q
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# from .forms import UserProfileUpdateForm, CustomUserCreationForm as CustomUserCreationForm, PasswordResetEmailForm, PasswordRecoveryForm,UserLoginForm,ChnagePassword
from django.views.decorators.csrf import csrf_protect,csrf_exempt
# from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, date ,time,timedelta
from django.views.generic import DetailView, ListView
from users.forms import *
from .properties import USAState
from django.contrib.auth import get_user_model

User = get_user_model()

@login_required
def log_out(request):
    logout(request)
    return HttpResponseRedirect("/")

def login(request):
    next_url = request.GET.get('next',False)
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse("user:profile"))
    form = UserLoginForm()
    if request.method=='POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            new_user = authenticate(username=username, password=password)
            if new_user is not None:
                if new_user.is_active:
                    user_login(request, new_user)
                    if not new_user.is_verified:
                        new_user.is_verified = True
                        new_user.save()
                    if next_url:
                        return HttpResponseRedirect(next_url)
                    else:
                        return HttpResponseRedirect(reverse("user:profile"))
                else:
                    messages.warning(request, 'Your account is not activeted, please activate your account!')
            else:
                messages.warning(request, 'Login email and password are incorrect!!')
    return render(request, "users/login.html",{'form':form, 'next':next_url})

@login_required
def profile(request):
    if request.user.is_authenticated():
        if request.method == "POST":

            google_calendar_id = request.POST.get('google_calendar_id', None)
            first_name = request.POST.get('first_name', None)
            last_name = request.POST.get('last_name', None)
            email = request.POST.get('email', None)
            phone_number = request.POST.get('phone_number', None)
            address = request.POST.get('address', None)
            city = request.POST.get('city', None)
            state = request.POST.get('state', None)
            zip_code = request.POST.get('zip_code', None)

            user_object = {
                'first_name': first_name,
                'last_name': last_name,
                'phone_number': phone_number,
                'address': address,
                'city': city,
                'state': state,
                'zip_code': zip_code,
                'google_calendar_id': google_calendar_id
            }

            CustomUser.objects.filter(id=request.user.id).update(**user_object)

        USA_states = USAState.USA_states
        profile_info = User.objects.get(id=request.user.id)
                
        return render(request, 'users/user_profile.html', {
            # 'database_list': db_list, 
            'profile': profile_info,
            'states': USA_states,
        })

# @login_required
@csrf_protect
def create_user(request):
    """
    Well... create a new user
    """
    # if request.user.is_superuser:
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            try:
                user.send_activation_email.delay(user_id=user.pk,request=request)
            except:
                user.send_activation_email(request)

            return render_to_response("users/verification_sent.html",
                RequestContext(request, {'email': user.email}))
        else:
            return render(request, "users/register.html",{'register_form':form})

    else:
        form = CustomUserCreationForm()
        return render(request, "users/register.html",{'register_form':form})

    # else:
    #     return HttpResponseRedirect(reverse("user:login"))

def resend_verification_mail(request):
    email = request.GET.get('email')
    user = get_user_model().objects.get(email=email)
    try:
        user.send_activation_email(request)
        return HttpResponse(json.dumps(dict(status='send')), mimetype="application/json")
    except:
        return HttpResponse(json.dumps(dict(status='try again')), mimetype="application/json")

@login_required
def create_profile(request):
    '''
    Create a new profile. This is just intended for new
    users. Return the profile edit page otherwise
    '''
    if request.method == "POST":
        form = UserProfileUpdateForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            user = form.save()
            return HttpResponseRedirect(reverse("user:profile"))
    else:
        
        form = request.user.is_authenticated()\
        and UserProfileUpdateForm(instance=request.user)\
        or UserProfileUpdateForm()

    return render_to_response("users/userprofile.html",
                              RequestContext(request, {"form": form}))

def activate(request, pk=None):
    """
    And today on... Mario uses exceptions as control flow!
    TODO: STOP DOING THAT
    """
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    try:
        user = get_user_model().objects.get(pk=pk)
    except User.DoesNotExist:
        raise Http404

    if not user.is_active:
        activation_code = request.GET.get("q", None)
        if activation_code and user.verification_code == activation_code:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            user_login(request, user)
            user.is_verified = True
            user.is_active = True
            user.save()
        return HttpResponseRedirect(reverse("user:profile"))
    else:
        raise Http404

@login_required
def update(request):
    '''
    Update the profile of a given user
    '''
    if request.method == "POST":
        form = UserProfileUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("user:profile"))
            
    else:
        form = UserProfileUpdateForm(instance=request.user)

    return render_to_response("users/update.html",
                              RequestContext(request, {"form": form}))

@csrf_exempt
def request_password_reset(request):
    # Just... well.. display the email form
    if request.method == "POST":
        form = PasswordResetEmailForm(request.POST)
        if form.is_valid():
            try:
                user = get_user_model().objects.get(email=form.cleaned_data["email"])
                user.send_password_reset_email(request)
                return render_to_response('users/request_password_sent.html', RequestContext(request, {'user': user}))
            except get_user_model().DoesNotExist:
                form.errors['email']="wrong email"
                #raise Http404
    else:
        form = PasswordResetEmailForm()

    return render_to_response("users/request_password_reset.html", 
        RequestContext(request, {"form": form}))

@csrf_exempt
def reset_password(request, pk=None):
    # Check if code is correct
    if request.method == "POST":
        form = PasswordRecoveryForm(request.POST)
        if form.is_valid():
            try:
                user = get_user_model().objects.get(pk=pk)                
            except get_user_model().DoesNotExist:
                raise Http404

            if form.cleaned_data["activation_key"] == user.verification_code:
                user.set_password(form.cleaned_data["password2"])
                user.save()

                # Login renewed user
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                user_login(request, user)
                return HttpResponseRedirect("/")
                #return HttpResponseRedirect(reverse("account:read"))
            else:
                raise Http404
    else:
        form = PasswordRecoveryForm({"activation_key": request.GET.get("q")})

    return render_to_response("users/password_recovery_form.html", 
        RequestContext(request, {"form": form, 'pk': pk}))

@csrf_exempt
@login_required
def changepassword(request):
    form = ChnagePassword()
    password_error = None
    if request.method == "POST":
        user = request.user
        form = ChnagePassword(request.POST)
        if form.is_valid():
            if form.check_password(user):
                user.set_password(form.cleaned_data["password2"])
                user.save()
                logout(request)
                return render_to_response('users/changepassword_done.html', RequestContext(request, {}))
            else:
                password_error = "wrong password"
                
    return render_to_response("users/changepassword.html", 
        RequestContext(request, {"form": form,'password_error':password_error }))

@login_required(login_url='/user/login/')
def delete(request):
    '''
    Delete user profile
    '''
    get_user_model().objects.get(user=request.user).delete()
    return HttpResponseRedirect("/")


class UpdateUserProfileView(DetailView):
    model = CustomUser
    form_class = UserProfileForm
    template_name = 'users/user_profile.html'

    def get_context_data(self, **kwargs):
        context = super(UpdateUserProfileView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()
        return context
        
    def post( self, **kwargs):
        context = {}
        if self.request.method == 'POST':
            form = self.form_class(self.request.POST)
            form.actual_user = self.request.user
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('user:profile'))
        else:
            form = self.form_class()

        context['form'] = form
        return render(self.request, 'users/user_profile.html', context)
