from django import forms

from .models import *

class CreateCampaignForm(forms.ModelForm):

    # title = forms.CharField(max_length=100, min_length=20)

    class Meta:
        model = Campaign
        exclude = ('twilio_cid','created_by', 'prospect','location_geocoords',)

    def __init__(self, *args, **kwargs):
        super(CreateCampaignForm, self).__init__(*args, **kwargs)

        for fields in self.fields:
        	self.fields[fields].widget = forms.TextInput(attrs={
            'class': 'form-control'})

