from django.shortcuts import render
from django.views.generic import ListView, UpdateView
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect, Http404, HttpResponse
from .models import *
from .forms import *
from prospect.models import *
from django.core.urlresolvers import reverse

# Create your views here.
class CampaignListView(ListView):
    """docstring for CampaignListView"""
    model = Campaign
    template_name = 'campaign/campaign_list.html'
    context_object_name = 'campagns'
        
class CampaignCreate(CreateView):
    model = Campaign
    form_class = CreateCampaignForm
    template_name = 'campaign/create_campaign.html'
    # success_url = reverse("campaign:campaign-list")

    def get(self, request, **kwargs):
        context = {}
        if request.user.is_authenticated() and request.user.is_superuser:
            prospect_obj = Prospect.objects.all().values()[:1]
            prospects_fields = []

        for po in prospect_obj:
            for k,v in po.items():
                prospects_fields.append(k)
        context['prospects_fields'] = prospects_fields

        return render(request, self.template_name, {'context':context })

    def post(self, request, **kwargs):
        form = self.form_class(request.POST)
        self.form_valid(form)
        return HttpResponseRedirect(reverse("campaign:campaign-list"))

    def form_valid(self, form):
        if form.is_valid():
            form.instance.created_by = self.request.user
            form.save()
            return super(CampaignCreate, self).form_valid(form)
        else:
            return render(request, self.template_name, {'form':form })
            

class CampaignUpdate(UpdateView):
    model = Campaign
    form_class = CreateCampaignForm
    template_name = 'campaign/create_campaign.html'

    def get_object(self):
        return self.model.objects.get(pk=self.kwargs['pk'])
