from django.conf.urls import url
from campaign.views import *

urlpatterns = [
    url(r'^$', CampaignListView.as_view(), name='campaign-list'),
    url(r'^create/$', CampaignCreate.as_view(), name='campaign-create'),
    url(r'^edit/(?P<pk>\d+)/$', CampaignUpdate.as_view(), name='campaign-update'),
]