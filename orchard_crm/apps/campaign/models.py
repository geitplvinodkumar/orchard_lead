from django.db import models
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.conf import settings
from prospect.models import *
from django.core.urlresolvers import reverse
# Create your models here.

class Campaign(AbstractCommon, models.Model):
    """
    Description: Model Description
    """
    alternative_phone_number = models.CharField(max_length=255)
    call_result = models.CharField(max_length=255)
    call_result_description = models.CharField(max_length=255)
    renewal_date = models.DateField(max_length=255, null=True, blank=True)
    email = models.EmailField(max_length=255)
    comments = models.CharField(max_length=255)

    sic_code = models.CharField(max_length=255)
    sic_code_description = models.CharField(max_length=255)
    naics_code = models.CharField(max_length=255)
    naics_code_description = models.CharField(max_length=255)

    carrier_of_record = models.CharField(max_length=255)
    lcm = models.CharField(max_length=255)

    employee_size = models.CharField(max_length=255)
    website = models.CharField(max_length=255)
    square_footage = models.CharField(max_length=255)
    location_geocoords = models.PointField(null=True, blank=True,)
    credit_rating = models.CharField(max_length=255)
    last_appointment_with = models.CharField(max_length=255)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user')
    prospect = models.ManyToManyField(Prospect)
    twilio_cid = models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse("campaign:campaign-list")