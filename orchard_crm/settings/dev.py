from .base import *

SECRET_KEY = 'n+u%u7b1-pxw+jl9_qng9(elve+z_a*@f#__rwpc%reg%^zp6m'

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'orchardcrm_dev',
        'USER': 'postgres',
        'PASSWORD': '123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}


DEFAULT_EMAIL_FROM = 'info@orchardcrm.com'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'kingap5891@gmail.com'
EMAIL_HOST_PASSWORD = 'kingap8982299456'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': [
            '37.187.151.11:11211',
            '37.187.151.11:11212',
            '37.187.151.11:11213',
        ]
    }
}

AUTH_PROFILE_MODULE = "users.CustomUser"
AUTH_USER_MODEL = "users.CustomUser"



MEDIA_ROOT = os.path.join(os.path.join(PROJECT_DIR, 'orchard_crm'), "media")
MEDIA_URL = '/media/'

# STATIC_ROOT =  os.path.join(os.path.join(PROJECT_DIR, 'orchard_crm'), "static")
STATIC_URL = '/static/'

LOGIN_URL = '/user/login/'

STATICFILES_DIRS = (
    os.path.join(os.path.join(PROJECT_DIR, 'orchard_crm'), "static"),
)